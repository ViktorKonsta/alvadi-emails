$ = require './auto-require'

template = (msg) -> "\n \n \t #{msg} \n"

module.exports =
	error: (project) ->
		$.util.log $.util.colors.bold.red template "(Error) No such project sources in ./source/#{project}/"
		do $.util.noop
	success: (project) ->
		$.util.log $.util.colors.green template "(OK) Task sources in ./source/#{project}/ were found. So let's do something awesome!"