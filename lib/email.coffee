$ = require '../lib/auto-require'
cfg = require '../lib/config'
path = require 'path'

me = 'viktor.konsta@gmail.com'
ivan = 'ivan.tuzov@alvadi.ee'
evgeny = 'evgeny@alvadi.ee'

makeList = (people) ->
	if not people? then return console.error 'No people to send!'
	return people.join(', ')

createTransporter = ->
	$.nodemailer.createTransport
		service: 'Gmail'
		auth:
			user: 'viktor.konsta@gmail.com'
			pass: '2013kvk190395kvk2013'

createOptions = (project) ->
	fs = require 'fs'
	options =
		from: 'ViktorKonsta <viktor.konsta@gmail.com>'
		to: makeList [me, ivan]
		subject: "This is '#{project}' email."
		html: fs.readFileSync path.resolve __dirname, '../', "./#{cfg.get('build')}/#{project}/index.html"

sendEmail = (transporter, options) ->
	transporter.sendMail options, (err, info) ->
		if err then $.util.log $.util.colors.bold.red err
		else $.util.log $.util.colors.green "Message sent: #{info.response}"

entry = (project) ->
	transporter = do createTransporter
	emailOptions = createOptions project
	sendEmail transporter, emailOptions

module.exports = entry