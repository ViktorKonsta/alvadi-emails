nconf = require 'nconf'
path = require 'path'

module.exports = nconf.file path.resolve __dirname, '../', 'config.json'