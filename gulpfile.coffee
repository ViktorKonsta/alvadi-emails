$ = do require 'auto-require'
fs = require 'fs'
cfg = require './lib/config'
message = require './lib/message'
emailSend = require './lib/email'

do $.browserSync.create

project = process.argv[2]
flag = process.argv[3]

$.gulp.task project, -> checkTask project

checkTask = (project) ->
	taskFolder = fs.readdirSync(cfg.get('source')).join('').match(project)
	if taskFolder?
		message.success project
		taskInit project
	else 
		message.error project

taskInit = (project) ->
	if flag?
		switch flag
			when '--send', '-s'
				$.gulp.start "#{project}:send"
			when '--build', '-b'
				$.gulp.start "#{project}:build"
		return
	$.gulp.start "#{project}:dev"

reload = -> do $.browserSync.stream

# Flows
$.gulp.task "#{project}:dev", ->
	$.runSequence "#{project}:compile", "#{project}/server/setup", "#{project}:watch"

$.gulp.task "#{project}:build", ->
	$.del("#{cfg.get('build')}/#{project}/").then ->
		$.runSequence "#{project}:compile", "#{project}/html/inline"

$.gulp.task "#{project}:send", ->
	$.gulp.start "#{project}:build"
	emailSend project

$.gulp.task "#{project}:watch", ->
	$.runSequence ["#{project}/jade/watch", "#{project}/images/watch", "#{project}/stylus/watch"]

$.gulp.task "#{project}:compile", ->
	$.runSequence "#{project}/jade/compile", "#{project}/images/compress", "#{project}/stylus/compile"

# Atoms
$.gulp.task "#{project}/server/setup", ->
	$.browserSync.init server: baseDir: "#{cfg.get('dist')}/#{project}/"

$.gulp.task "#{project}/html/inline", ->
	$.gulp.src "#{cfg.get('dist')}/#{project}/*.html"
		.pipe do $.plumber
		.pipe $.inlineCss
			preserveMediaQueries: yes
			applyWidthAttributes: yes
			applyTableAttributes: yes
		.pipe $.gulp.dest "#{cfg.get('build')}/#{project}/"

$.gulp.task "#{project}/images/compress", ->
	$.gulp.src "#{cfg.get('source')}/#{project}/images/*"
		.pipe $.imagemin progressive: on
		.pipe $.gulp.dest "#{cfg.get('dist')}/#{project}/images/"
		.pipe $.gulp.dest "#{cfg.get('build')}/#{project}/images/"
		.pipe do reload

$.gulp.task "#{project}/images/watch", ->
	$.watch "#{cfg.get('source')}/#{project}/images/*", ->
		$.gulp.start "#{project}/images/compress"

$.gulp.task "#{project}/jade/compile", ->
	$.gulp.src "#{cfg.get('source')}/#{project}/jade/*.jade"
		.pipe do $.plumber
		.pipe $.jade pretty: true
		.pipe $.gulp.dest "#{cfg.get('dist')}/#{project}/"
		.pipe do reload

$.gulp.task "#{project}/jade/watch", ->
	$.watch "#{cfg.get('source')}/#{project}/jade/**/*.jade", ->
		$.gulp.start "#{project}/jade/compile"

rucksackBundle =
	$.rucksackCss
		autoprefixer: true
		fallbacks: true

$.gulp.task "#{project}/stylus/compile", ->
	$.gulp.src "#{cfg.get('source')}/#{project}/stylus/index.styl"
		.pipe do $.plumber
		.pipe $.stylus 
			'include css': true
			use: [ $.poststylus [rucksackBundle] ]
		.pipe $.gulp.dest "#{cfg.get('dist')}/#{project}/css/"
		.pipe do reload

$.gulp.task "#{project}/stylus/watch", ->
	$.watch ["#{cfg.get('source')}/#{project}/stylus/**/*.styl", "#{cfg.get('tommy')}/**/*.styl"], ->
		$.gulp.start "#{project}/stylus/compile"